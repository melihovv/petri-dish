package melihovv.PetriDish.fieldObjects;

/**
 * The class represents static field object which can't move and do anything.
 */
public abstract class InactiveFieldObject extends FieldObject {
    /**
     * The basic constructor for class members initialization.
     */
    public InactiveFieldObject() {
        super();
    }
}
